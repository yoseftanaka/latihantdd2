package com.binus;

import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoListTest {

    @Test
    public void testAddTodoToList(){
        String expecetdTodo = "Ngoding";

        ToDoList toDoList = new ToDoList();
        toDoList.addToDo("Noding");
        assertEquals(expecetdTodo,toDoList.getToDo(0));
    }

    @Test
    public void testRemoveTodoToList(){
        ToDoList toDoList = new ToDoList();
        toDoList.addToDo("Ngoding");
        toDoList.addToDo("Baca");
        toDoList.addToDo("Tidur");
        toDoList.removeTodo(1);
    }
}