package com.binus;

import java.util.ArrayList;

public class ToDoList {
    private ArrayList<String> todos;

    public ToDoList(){
        this.todos = new ArrayList<>();
    }

    public void addToDo(String newToDo){
        todos.add(newToDo);
    }

    public String getToDo(int index){
        return todos.get(index);
    }

    public void removeTodo(int index){
        todos.remove(index);
    }

    public int getTodosListLength(){
        return todos.size();
    }
}
